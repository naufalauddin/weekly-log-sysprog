# Week 5

Minggu kemaren aku tidak sempat mengerjakan pre test lagi, namun setidak
hari ini bisa mengumpulkan worksheet 5.

## Worksheet 5 dan Apa yang Kudapatkan

Pada pengerjaan worksheet 5 ini, aku belajar banyak mengenai memori dan
*memory allocation*. Dimulai dengan pemahaman *virtual memory*, yaitu
sebuah metode alokasi memori dengan cara memanfaatkan *hard disk*
sebagai ekstensi dari RAM. Penempatan data-data di memori ke swap di
*disk* semuanya diatur oleh kernel.

Penggunaan *Virtual Memory* ada baik dan buruknya. Baiknya dengan adanya
*virtual memory*, lebih banyak program dapat dijalankan bersamaan,
karena ukuran memori menjadi lebih besar karena memanfaatkan *hard disk*.
Buruknya, karena sebagian menggunakan *hard disk*, maka bisa lebih lambat
karena mengakses *hard disk* jauh lebih lambat dari mengases RAM.

Selain *virtual memory*, saya juga belajar tentang *memory leak*, yaitu
dimana kondisi penggunaan resource yang tidak maksimal karena manajemen
memori yang tidak baik. Hal tersebut lebih rentan terjadi pada bahasa
pemrograman yang tidak memiliki *garbage collector*, jadi harus bisa
mengalokasikan sendiri memori dan menghapusnya jika tidak perlu. Pada
bahasa pemrograman lain juga dapat terjadi, namun akan lebih susah
untuk di-*handle* karena semua penghapusan memori diserahkan ke *garbage
collector*.

Setelah itu saya juga belajar tentang `malloc` yaitu cara untuk
menambahkan alokasi memori di C. `malloc` akan memperbesar *heap memory*
dari sebauh program untuk menambahkan alokasi tersebut. Dalam pembesaran
tersebut dapat terjadi penambahan page atau menggunakan page terakhir.

Selain itu ada `jump` yang loncat ke sebuah line dalam C.
